// Accordions
const acc = document.getElementsByClassName('accordion__button');
const accordLength = acc.length;

for (let i = 0; i < accordLength; i += 1) {
    acc[i].addEventListener("click", (e) => {
        e.preventDefault();
        acc[i].parentElement.classList.toggle('open');
        const info = acc[i].nextElementSibling;
        if (info.style.height) {
            info.style.height = null
        } else {
            info.style.height = info.scrollHeight + "px";
        }
    });
}

window.addEventListener('resize', () => {
    const openedAccords = document.querySelectorAll('.accordion__item.open');
    const openAccordsLength = openedAccords.length;
    for (let i = 0; i < openAccordsLength; i += 1) {
        const accordInfo = openedAccords[i].querySelector('.accordion__info');
        const infoHeight = accordInfo.style.height;
        const contentHeight = accordInfo.querySelector('.container').scrollHeight + 'px';
        if (infoHeight !== contentHeight) {
            accordInfo.style.height = contentHeight;
        }
    }
});


// visit counter
let counter = parseInt(localStorage.getItem('visitCount'));
if (counter) {
    localStorage.setItem('visitCount', ++counter);
} else {
    counter = 1;
    localStorage.setItem('visitCount', counter);
}

const counterElement = document.querySelector('.stats__transactions .stat__data');
if (counterElement) {
    counterElement.innerHTML = counter;
}


// nav
const urls = document.querySelectorAll('a[data-url]');
urls.forEach(url => {
    url.addEventListener('click', (e) => {
        e.preventDefault();
        const address = url.getAttribute('data-url');
        history.pushState({ url: address }, null, address);
        fetch(address + '.html').then(res => {
            if (res.status === 200) {
                return res.text();
            }
        }).then(page => {
            if (page) {
                document.querySelector('html').innerHTML = page;
            }
        });
    });
})

window.onpopstate = (e) => {
    history.replaceState({ url: '/sol-wallet/' }, null);

    fetch(history.state.url).then(res => {
        if (res.status === 200) {
            return res.text();
        }
    }).then(page => {
        if (page) {
            document.querySelector('html').innerHTML = page;
        }
    });
}


const hamburger = document.querySelector('.main-header__hamburger.show-for-small')
hamburger.addEventListener('click', function () {
    hamburger.classList.toggle('active');
    document.querySelector('.main-header__hamburger-menu').classList.toggle('hide');
});



// dashboard
const graphs = {
    data: [],
    draw: () => {
        graphs.data.forEach(graph => {
            const ctx = graph.ctx;
            const data = graph.history;
            const maxValue = graph.maxValue;
            const container = graph.container;

            if (ctx.canvas.width !== container.offsetWidth) {
                ctx.canvas.width = container.offsetWidth;

                const gradient = ctx.createLinearGradient(0, 0, 0, 48);
                gradient.addColorStop(0, '#fbd6a6');
                gradient.addColorStop(1, '#fff');
                ctx.fillStyle = gradient;
                ctx.strokeStyle = '#FF901A';
                ctx.lineWidth = 4;
                ctx.lineCap = 'round';

                const spaceBetween = ctx.canvas.width / (data.length - 1);

                let currentPointX = 0
                const calculatePos = value => {
                    const percentage = Math.floor(((maxValue - value)) / maxValue * 100) * 0.01;
                    const x = currentPointX;
                    const y = (ctx.canvas.height - 2) * percentage + ctx.lineWidth / 2;

                    currentPointX += spaceBetween;

                    return {
                        x: x,
                        y: y
                    }
                };

                const firstVal = data.pop();
                const firstPos = calculatePos(firstVal);
                ctx.moveTo(firstPos.x, firstPos.y);

                for (let index = data.length - 1; index >= 0; index -= 1) {
                    let currentValue = data[index];
                    let currentPoint = calculatePos(currentValue);
                    ctx.lineTo(currentPoint.x, currentPoint.y);
                }

                ctx.stroke();
                ctx.lineTo(ctx.canvas.width, ctx.canvas.height);
                ctx.lineTo(0, ctx.canvas.height);
                ctx.fill();
            }
        });
    }
}

if (document.querySelector('.dashboard')) {
    fetch('https://testsolapi2.hivechat.im/general/dashboard')
        .then(res => res.json())
        .then(data => {
            const dashboard = data.result.dashboard;

            const date = new Date;
            const day = date.toLocaleString('en-US', { day: '2-digit' });
            const dateTo = day + ' ' + date.toLocaleString('en-US', { month: 'short' });
            date.setMonth(date.getMonth() - 1);
            const dateFrom = day + ' ' + date.toLocaleString('en-US', { month: 'short' });

            dashboard.rates.forEach(rate => {
                const pairName = rate.pair.toLowerCase().replace('/', '-');
                let history = rate.history;
                let currentRate = rate.current
                history.unshift(currentRate);
                const minValue = history.reduce((acc, curr) => {
                    return Math.min(acc, curr);
                })

                history = history.map(value => {
                    return value - minValue;
                });

                const maxValue = history.reduce((acc, curr) => {
                    return Math.max(acc, curr);
                });

                if (currentRate < 1) {
                    currentRate = Number(currentRate).toFixed(4);
                } else {
                    currentRate = Number(currentRate).toFixed(2);
                }

                const canvas = document.getElementById(pairName);
                const container = canvas.parentElement.parentElement;
                container.querySelector('.exchange__value').innerText = currentRate;
                container.querySelector('.period__from').innerText = dateFrom;
                container.querySelector('.period__to').innerText = dateTo;

                const ctx = canvas.getContext('2d');
                graphs.data.push({ ctx, history, maxValue, container });
            });

            document.querySelector('.stats__users .stat__data').innerText = dashboard.active_users_count;
            document.querySelector('.stats__availability .stat__data').innerText = dashboard.month_availability + '%';

            const infoItems = document.querySelectorAll('.sys-info .sys-info__item');
            const statuses = ['system_status_blockchain', 'system_status_payments_engine', 'system_status_mobile_apps'];
            for (let index in statuses) {
                if (dashboard[statuses[index]]) {
                    infoItems[index].classList.add('online');
                }
            }

            document.querySelector('.dashboard__wrapper.hidden').classList.remove('hidden');
            graphs.draw();

        });


    window.addEventListener('resize', () => {
        graphs.draw();
    }, { once: true });
}
